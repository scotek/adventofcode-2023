# adventofcode-2023

My Advent of Code 2023 attempts.  Languages may vary.

Play along at <https://adventofcode.com/2023>

This repo at <https://gitlab.com/scotek/adventofcode-2023>

# Day 01 `idris/Day01.idr`

## Task 1: 24m

(55488)

Trivial (if you think of the `reverse`) but the `find` immediately hit the bane of AoC - dealing with `Maybe`s everywhere.  Started to do it with `<*>` then realised the individual `Char`s would need to be converted to numbers too and that looked ugly either inside or after the other `Maybe`s... So I just switched to `do`-notation to do it step by step.  The real pain point here is when you know the input file will have certain invariants but you can't take advantage of them because Idris doesn't know.  In this input, it's that each line will have two numbers and every line will have two numbers.  If Idris knew that then no `Maybe`s would be needed.


## Task 2: 40m

(55614)

There doesn't seem to be either a string or a generic like find&replace/substitute function in the stdlib so I had to write one.  The problem is pretty trivial as long as you think to covert it into the first part by replacing the text with numbers but I can imagine the actual implementation is quite fiddly in quite a few languages if it's not in their stdlibs either.  This is quite awkward compared to most day 1 puzzles - looking at the stats there is a huge reduction in the number of people completing both tasks compared to normal!

# Day 02 `idris/Day02.idr`

## Task 1: ~130m

(2162)

Watching videos at the same time is not conducive to work.  Parsing the input at the beginning was a bit annoying.  Also did a bit too much work working out whole match completion that I didn't really need as the calculations where all within-game.  I had to make a lot of the functions partial because `strTail` is partial but that isn't shown in the REPL `:doc` command, which confused me for a while.

### Wrong Attempts

~120m. 1498. Had `<` instead of `<=` in the exceeds check.

## Task 2: 8m

(72513)

Pretty straight-forward to do the second part since I bothered to load everything in in task 1.


# Day 03 `idris/Day03.idr`

## Task 1: ~120m

(559667)

Probably took 45-60 mins of actual work, minus about 10 mins of thinking about the design.  The first obvious way to do it, playing directly around with a grid of chars, is a bit pain in Idris.  A bit of thinking shows the important thing is coordinates of the numbers and symbols then everything could be worked out afterwards, so the parsing stage builds a list of "items" (number or symbol) by scanning through each line char by char creating an item for each char sequence that isn't '.' that is seen.  Then to solve the problem, get all the symbols, for each of them check against all the items for coordinate intersection of the 8 points around the symbol and the coords of each number.  Make sure not to return a number more than once if more than one coord of the number is adjacent.  Then get the numbers and add them all up.

## Task 2: 14m

(86841457)

This was more interesting.  Made it worthwhile to do the bits before.  All that was really left to do was mess around a bit making sure that the length-2 list was kept as a length-2 list rather than prematurely flattened so they can be multiplied appropriately before the sum.  Got both parts correct first time which was nice.


# Day 04 `idris/Day04.idr`

## Task 1: 45m

(23235)

Straight-forward but got distracted by the fact there doesn't seem to be a way to convert from Vect to List in the stdlib?  Spent a long time looking because surely I've just forgotten it and can't find it again - there must be a way to do it surely?

## Task 2: ~20m

(5920640)

The problem spec read bizarrely.  I gave it a read then just left it and went to bed. Four days later I had time to come back to it and it made a bit more sense.  Once I realised it was really just a carry-forward of the match count to the next *n* cards, I just had to build a list and function to update a certain sized list prefix of that list and then go down the cards.  90% of the problem here was understanding what it meant.

# Day 05 `idris/Day05.idr`

## Task 1: 152m

(486613012)

12 minutes just to read the problem spec but after that it wasn't particularly difficult, just a bit boring so I kept getting distracted by videos again.  None of the problems so far this year are really grabbing me - it's mostly futzing around with input files formatted more fancily than normal.

### Wrong Attempts

125m. 1600322402, too high. Took 16 mins to realise I forgot the bit of the problem spec to pass numbers straight through if they don't fall within one of the spans.  Then I realised I still wasn't getting the values I expected through so another 11 mins of debugging and realised I was passing the original value `i` recursively through `translate'` instead of the value `s` computed at each step.  Then it immediately all worked.

## Task 2: ~66m

(56931769)

This part was a bit more interesting!  The first gotcha of the year where the straight-forward implementation won't run quickly enough to complete.  Mapping ranges backwards (with splitting/joining ranges) and then taking the lowest of that range seems the proper solution but I decided to see if it was possible to just brute-force backwards from location 0, and it worked.  Took about 210s to find the first valid location value.  This is a surprisingly o approach several times a year - set the brute force method running while writing the smarter version.  I'm still a bit surprised just how slow it goes though.  Sorting the span maps and using Vect instead of List are the only non-algorithmic optimisations that immediately jump out.

### Wrong Attempts

59m. 3876934459, too high.  Pretty straight-forward, just was returning the initial seed value instead of the output location value.

# Day 06 `idris/Day06.idr`

## Task 1: 21m

(500346)

Nice little puzzle, quick to do.  Would have preferred to start off AoC with something like this rather than the "input format" challenges.

## Task 2: 16m

(42515755)

Interesting extension.  Just as straight-forward as it seems.  I wonder if it was to try to play a little bit of a trick to make people think brute force wouldn't work like the last problem.


# Day 07 `idris/Day07.idr`

## Task 1: ~385m

(248569531)

This took a while but overall was quite a fun puzzle to do.  Definitely highlighted the areas where there is a lot of verbosity in the language.  A bit annoying I also had to end up doing `Show` instances for everything since it's time consuming; also just dumping everything and looking over it feels like defeat compared to thinking it through.

Probably about ~180-240m of actual work.  `extractType` is the real workhorse and was relatively quick; all the boilerplate around it of `Ord` (and therefore `Eq`) instances was an annoying pain.  Was trying to do the hand as a `Vect 5 Char` for a while but it never really paid off so just swapped it over to the `List Char` that I was converting it into in most places.  I'm sure I've seen poker packages around for several languages in the past (it's a common textbook case study after all, and a common programming puzzle task) so they might have got a speed up.  It never felt quite worth moving away from characters and lists entirely though.  Of course, who knows what task 2 is going to ask...

### Wrong Attempts

~330m. 248741848, too high.  Took about 30 mins from this to work out the initial sort wasn't happening before calling `extractType`.  I had a small function which did a sort and then called `extractType` with the sorted version, and it was right above my main task function so I could always see it being passed correctly.  Eventually I realised I just wasn't using that small function anymore and had changed it to call `extractType` directly during a refactor - a good example of why you shouldn't leave dead code around (especially uncommented).  I'm usually on top of deleting things during refactors; presume it was just because I've been doing random things all day interspersed with this.

357m. After sorting the above something was still wrong without submitting so I added more and more debug printing, which I really try to avoid as it's so blunt and spoils the fun of the puzzle (and is annoying to write all the `Show` instances).  It showed the small example in the correct order, and my own test input in the correct order, but running the full input showed the OnePair and HighCard results were being interspersed.  From there it was immediate, the `compare OnePair _` case was `LT` when it should have been `GT`.  Missed in debugging because I must have not had a OnePair and HighCard in that order and not HighCard and OnePair.  I was assuming it was the secondary test of the order of cards in the hand that was probably the culprit and mainly looking there.

## Task 2: 70m

(250382098)

Interesting!  This is why I start to commit between tasks because some of them, like this, seem easier to implement by just changing the existing program rather than writing mostly similar separate functions.  I prefer both tasks to work at the same time though so let's see how it goes...[Ed: I ended up just refactoring slightly to allow both versions to work in parallel because it's tidier and I'm not in a rush.  Would have avoided 2 bugs with editing but 1 would have still happened.]

I quite liked these two puzzles.  Once you group them into sub-lists of the same card then it's just sitting down and pondering a bit to see what possible hand types could occur from *n* groups of cards of given lengths.  Task 2 was then easy to do because I had all the bits just to be able to brute-force evaluate the 12 possible hands for J substitution and see which ranks highest.  The only other bit needing changed was to update the comparison, which gave me a chance to use Named Implementations in Idris for the first time.  A little bit of extra thinking was required to confirm that splitting J's into more than one kind of card would never be a better outcome, particularly around Full House; it's always better giving them to the largest existing group of cards as any permutation in which you could get to a full house would also get you a higher type on just one of the cards.

So, all in all, 25m of debugging and identified 3 bugs that all would have needed fixes in whatever order they were discovered.

### Wrong Attempts

45m. 249685763, too low.  Due to missing using the task 2 `Ord` interface in a sort.

47m. 249861328, too low.  Eventually realised that `extractType` is also using the card order for high card so also need to give that as a parameter to be correct.  However, the answer doesn't change, which makes me think something else hasn't been updated for the new task.

63m. 249861328, too low.  Final bug fix needed was that I forgot to sort the hands after doing the `replaceOn` to generate the hand permutations!  That means similar cards weren't adjacent so the `group` call at the start of `extractType` didn't pull everything together correctly.

# Day 08 `idris/Day08.idr`

## Task 1: ~40m

(13939)

The first graph problem!  Hopefully it's going to pay off in task 2 to build a graph properly.  I wonder if it'll ask to search for the shortest route in task 2.  SortedMap is still the way to go in the stdlib for adjacency lists (especially in this scenarios where all the edges are known when the node is defined).

Had to do a bit of fiddling to go back and change line parsing to use Maybe then gave up at the end and just did partial stuff.  Pretty straight forward.  Bit disappointed how long I'm taking even thought I'm doing other things.  This should only take 15-20m from start to correct answer.  Slowed by endless fiddling with maybe's.

## Task 2: 65m (+16m)

(8906539031197)

Interesting extension.  Often I would have written `doSteps` as `doOneStep` and `doAllSteps` but I didn't do that this time...typical!  Was able to write the multi-path following approach relatively simply but it didn't complete after a few minutes.  If we assume that the end of the combined journeys for task 2 is a multiple of each individual trip then we can just calculate the LCM but I don't think it actually says that anywhere.  LCM is really slow in Idris since it works on Nat's so I calculated the lengths individually and threw it into Wolfram Alpha to calculate the LCM of them all to see if it was the right answer, and it was!  So I then spent a little bit extra time to write `Int` equivalents of the `Nat` versions of `lcm` and `gcd` that are in the stdlib.  I feel like this is the second task I've had to make a little unstated assumption this year but I can't remember which previous task it was.  Maybe I'm just super picky about stating things that are safe to assume after years and years of writing assessment questions!

# Day 09 `idris/Day09.idr`

## Task 1: 36m

(1972648895)

I'm sure there is a way to calculate this series of arithmetic progressions but I can't remember the proper name so I can't find it... The input file only has 21 entries so I'll just do it brute force and no-doubt task 2 will require a smarter solution!

## Task 2: 15m

(919)

Nice to have a simple extension now and then.  Just had to do the same things as the first task but at the other side of the triangle with subtraction instead of addition.

