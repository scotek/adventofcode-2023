module Day08

import System
import AOCUtils
import Data.List
import Data.List1
import Data.Maybe
import Data.String
import Data.String.Extra
import Data.SortedMap

||| Alias to for clarity and to reduce typing.
Graph : Type
Graph = SortedMap String (List String)

record Network where
  constructor MkNetwork
  path : List Char
  graph : Graph

||| Convert the header into steps.  Might be more complicated in task 2.
parseHeader : String -> List Char
parseHeader = unpack

||| Parse an individual graph line into the src and list of dests.
parseLine : String -> Maybe (String, List String)
parseLine str = do let ws = words str
                   from <- getAt 0 ws
                   d1 <- getAt 2 ws
                   let d1' = shrink 1 d1
                   d2 <- getAt 3 ws
                   let d2' = take 3 d2
                   Just (from, [d1', d2'])

||| Convert the body into a graph.
parseBody : List String -> Graph
parseBody strs = parseBody' strs empty
  where
    parseBody' : List String -> Graph -> Graph
    parseBody' [] g = g
    parseBody' (x :: xs) g = let n = parseLine x
                                 g' = case n of
                                           Just (from, to) => insert from to g
                                           Nothing => g
                             in parseBody' xs g'

||| Read the entire Network file.
parseFile : List String -> Network
parseFile strs = let header = fromMaybe "" $ getAt 0 strs
                     body = drop 2 strs
                 in MkNetwork (parseHeader header) (parseBody body)

||| Step through the path, counting the steps, and finish when we reach '??Z'
partial
doSteps : (steps: List Char) -> (net : Network) -> (curPos: String) -> (accSteps: Int) -> Int
doSteps [] net pos i = doSteps net.path net pos i -- wrap around the instructions again.
doSteps (x :: xs) net pos i = let connections = fromMaybe [] $ lookup pos net.graph
                                  nxt = case x of
                                             'L' => getAt 0 connections
                                             _ => getAt 1 connections
                              in case nxt of
                                      Nothing => idris_crash "Can't find next connection"
                                      Just nt => case isSuffixOf "Z" nt of -- safe for T1 too.
                                                      True => i+1
                                                      False => doSteps xs net nt (i+1)


||| Follow the path through the network from AAA to ZZZ and return the step count.
partial
followPath : Network -> Int
followPath net = doSteps net.path net "AAA" 0

partial
task1 : List String -> Maybe Int
task1 strs = let net = parseFile strs
                 len = followPath net
             in Just len


||| Return the list of all nodes that end with 'A'.
identifyStartNodes : Network -> List String
identifyStartNodes net = let g = net.graph
                             ns = keys g
                             starts = filter (isSuffixOf "A") ns
                         in starts
                         
{- These functions follow multiple paths through the graph at the same
  time.  They work but changed implmentation to not use them.
  Current solution just does the routes individually and calculates
  the LCM of the number of steps.
  
||| Take one further step along the path from the given point.  Returns
||| the new node.
partial
doOneStep : (step: Char)-> (net: Network) -> (curPos: String) -> String -- newPos
doOneStep x net curPos = let connections = fromMaybe [] $ lookup curPos net.graph
                             nxt = case x of
                                        'L' => getAt 0 connections
                                        _ => getAt 1 connections
                         in case nxt of
                                 Nothing => idris_crash "Can't find next connection"
                                 Just nt => nt

partial
doMultiOneStep : (step: Char) -> (net: Network) -> (starts: List String) -> List String
doMultiOneStep x net starts = let g = net.graph
                                  nxts = map (doOneStep x net) starts
                              in nxts

partial
fmp : (steps : List Char) -> (net: Network) -> (curPos : List String) -> (acc: Int) -> Int
fmp [] net curPos acc = fmp net.path net curPos acc
fmp (x :: xs) net curPos acc = let nxts = doMultiOneStep x net curPos
                                   allEndPoint = all (isSuffixOf "Z") nxts
                               in case allEndPoint of
                                       True => acc + 1
                                       False => fmp xs net nxts (acc+1)
-}

||| Altered to just find the first stopping point for each route at the first '??Z'
||| and take the LCM of all those lengths.  It works, although I'm don't think
||| the problem says it's safe to stop at the first possible stop node (ie,
||| it doesn't forbid a route going over multiple stop nodes.
partial
followMultiPath : Network -> Int
followMultiPath net = let starts = identifyStartNodes net
                          lens = map (\x=> doSteps net.path net x 0) starts
                          len = foldr (fastLcm) 1 lens
                      in len

partial
task2 : List String -> Maybe Int
task2 strs = let net = parseFile strs
                 len = followMultiPath net
             in Just len

--------------------------------------------------------------------------------
partial
main1 : IO()
main1 = withLines' "day08-input.txt" task1

partial
main2 : IO()
main2 = withLines' "day08-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
partial
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
