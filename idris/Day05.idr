module Day05

import System
import AOCUtils
import Data.List
import Data.List1
import Data.Maybe
import Data.String
import Data.String.Extra

||| Destination start, Source Start, Span Length
data Span = MkSpan Int Int Int

||| Represent the entire input file.
record Almanac where
  constructor MkAlmanac
  seeds : List Int
  stages : List (List Span)     -- store stages in the order as the file

||| Translates a value through the given span or Nothing if out of span.
translateSpan : Int -> Span -> Maybe Int
translateSpan x (MkSpan dest src len) = if x >= src && x < (src+len)
                                        then
                                          let offset = x - src
                                          in Just (dest + offset)
                                        else
                                          Nothing

||| Translate a value through one of the given spans.
translateSpanList : Int -> List Span -> Maybe Int
translateSpanList i [] = Just i
translateSpanList i (x :: xs) = translateSpan i x <|> translateSpanList i xs

||| Given a number, try to translate it through all the levels available in the Almanac.
translateThroughAlmanac : Almanac -> Int -> Maybe Int
translateThroughAlmanac alm n = translate' alm.stages n
  where
    translate' : List (List Span) -> Int -> Maybe Int
    translate' [] i = Just i
    translate' (x :: xs) i = do s <- translateSpanList i x
                                translate' xs s


----- Backwards translation functions are for Part 2.

||| Try and translate an output back through a span.
backwardTranslateSpan : Int -> Span -> Maybe Int
backwardTranslateSpan x (MkSpan dest src len) = if x >= dest && x < (dest+len)
                                        then
                                          let offset = x - dest
                                          in Just (src + offset)
                                        else
                                          Nothing

||| Translate an output backwards through a stage.
backwardTranslateSpanList : Int -> List Span -> Int
backwardTranslateSpanList i [] = i
backwardTranslateSpanList i (x :: xs) = case backwardTranslateSpan i x of
                                             Just x => x
                                             Nothing => backwardTranslateSpanList i xs

||| Given a final destination number, translate it backwards to an input number.
||| Assuems the stages list has already been reversed before being passed in.
backwardTranslate : (revStages: List (List Span)) -> (output: Int) -> Int
backwardTranslate [] i = i
backwardTranslate (x :: xs) i = let i' = backwardTranslateSpanList i x
                                in backwardTranslate xs i'


----- File parsing functions.

||| Parse the seeds line from the top of an almanac.
parseSeeds : String -> List Int
parseSeeds str = let numsStr = drop 1 $ snd $ break (==':') str
                     nums = words numsStr
                     ns = mapMaybe parsePositive nums
                 in ns

||| Parse an individual line of a field map.
parseSpan : String -> Maybe Span
parseSpan str = do let parts = words str
                   n1Str <- getAt 0 parts
                   n2Str <- getAt 1 parts
                   n3Str <- getAt 2 parts
                   n1 <- parsePositive n1Str
                   n2 <- parsePositive n2Str
                   n3 <- parsePositive n3Str
                   Just $ MkSpan n1 n2 n3

||| Parse a field map, ignoring the label line and assuming all span lines parse correctly.
parseFieldMap : List String -> List Span
parseFieldMap strs = let (labelStr, spanStrs) = (take 1 strs, drop 1 strs)
                         spans = mapMaybe parseSpan spanStrs
                     in spans

||| Parse an entire almanac file.
parseAlmanac : List String -> Almanac
parseAlmanac strs = let parts = split (=="") strs
                        seedsPart = head parts
                        seeds = parseSeeds (fromMaybe "" (getAt 0 seedsPart))
                        fieldParts = tail parts
                        fields = map parseFieldMap fieldParts
                    in MkAlmanac seeds fields

task1 : List String -> Maybe Int
task1 strs = let almanac = parseAlmanac strs
                 sds = almanac.seeds
                 sds' = mapMaybe (translateThroughAlmanac almanac) sds
                 m = foldr min 99999999999 sds'
             in Just m

----- Task 2

||| Group pairs of elements. If there is an odd number of elements then the last element will be omitted.
group2 : List a -> List (a,a)
group2 [] = []
group2 (x :: []) = []
group2 (x :: (y :: xs)) = (x,y) :: group2 xs

||| Is the number within and of the seed ranges?
isWithinAnySeed : List (Int, Int) -> Int -> Bool
isWithinAnySeed [] i = False
isWithinAnySeed ((start, len) :: xs) i = if i >= start && i < (start+len) then True else isWithinAnySeed xs i

||| Give the stages in reverse, and the list of seeds, start at output 0 and search up until
||| a corresponding input is within one of the seed ranges.
||| Return that output.
findFirstOutput : (revStages: List (List Span)) -> (sortedSeeds: List (Int, Int)) -> (output: Int) -> Int
findFirstOutput revStages sortedSeeds output = let input = backwardTranslate revStages output
                                                   ok = isWithinAnySeed sortedSeeds input
                                               in case ok of
                                                       True => output
                                                       False => findFirstOutput revStages sortedSeeds (output+1)

task2 : List String -> Maybe Int
task2 strs = let almanac = parseAlmanac strs
                 psds = group2 almanac.seeds
                 psds' = sortBy compareFst psds
                 x = findFirstOutput (reverse almanac.stages) psds' 0
             in Just x

--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day05-input.txt" task1

main2 : IO()
main2 = withLines' "day05-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
