module Day07

import System
import AOCUtils
import Data.List
import Data.List1
import Data.Maybe
import Data.String
import Data.String.Extra
import Data.Vect
import Data.Vect.Sort

data HandType = FiveKind Char
              | FourKind Char
              | FullHouse Char Char -- Triple first
              | ThreeKind Char
              | TwoPair Char Char -- Highest pair first
              | OnePair Char
              | HighCard Char

Show HandType where
  show (FiveKind c) = "FiveKind " ++ (show c)
  show (FourKind c) = "FourKind " ++ (show c)
  show (FullHouse c d) = "FullHouse " ++ (show c) ++ " " ++ (show d)
  show (ThreeKind c) = "ThreeKind " ++ (show c)
  show (TwoPair c d) = "TwoPair " ++ (show c) ++ " " ++ (show d)
  show (OnePair c) = "OnePair " ++ (show c)
  show (HighCard c) = "HighCard " ++ (show c)

||| Eq instance which only cares about the kind, not the values of the card in the kind.
Eq HandType where
  (==) (FiveKind _) (FiveKind _)  = True
  (==) (FourKind _) (FourKind _)  = True
  (==) (FullHouse _ _) (FullHouse _ _) = True
  (==) (ThreeKind _) (ThreeKind _)  = True
  (==) (TwoPair _ _) (TwoPair _ _)  = True
  (==) (OnePair _) (OnePair _)  = True
  (==) (HighCard _) (HighCard _)  = True
  (==) _ _ = False

||| Ord instance which only cares about the kind, not the values of the card in the kind.
-- Wish there was a more succinct way to define this.
Ord HandType where
  compare (FiveKind c) (FiveKind d) = EQ
  compare (FiveKind c) _ = GT
  compare (FourKind c) (FiveKind d) = LT
  compare (FourKind c) (FourKind d) = EQ
  compare (FourKind c) _ = GT
  compare (FullHouse c d) (FiveKind c1) = LT
  compare (FullHouse c d) (FourKind c1) = LT
  compare (FullHouse c d) (FullHouse c1 d1) = EQ
  compare (FullHouse c d) _ = GT
  compare (ThreeKind c) (FiveKind d) = LT
  compare (ThreeKind c) (FourKind d) = LT
  compare (ThreeKind c) (FullHouse d c1) = LT
  compare (ThreeKind c) (ThreeKind d) = EQ
  compare (ThreeKind c) _ = GT
  compare (TwoPair c d) (FiveKind c1) = LT
  compare (TwoPair c d) (FourKind c1) = LT
  compare (TwoPair c d) (FullHouse c1 d1) = LT
  compare (TwoPair c d) (ThreeKind c1) = LT
  compare (TwoPair c d) (TwoPair c1 d1) = EQ
  compare (TwoPair c d) _ = GT
  compare (OnePair c) (FiveKind d) = LT
  compare (OnePair c) (FourKind d) = LT
  compare (OnePair c) (FullHouse d c1) = LT
  compare (OnePair c) (ThreeKind d) = LT
  compare (OnePair c) (TwoPair d c1) = LT
  compare (OnePair c) (OnePair d) = EQ
  compare (OnePair c) _ = GT
  compare (HighCard c) (HighCard d) = EQ
  compare (HighCard c) _ = LT

||| A way to keep all the parts of a hand together.
record Hand where
  constructor MkHand
  hand : List Char
  type : HandType
  bid : Int

Show Hand where
  show (MkHand hand type bid) = "(" ++ (pack hand) ++ " " ++ (show type) ++ " " ++ (show bid) ++ ")"

Eq Hand where
  (==) (MkHand hand type bid) (MkHand h t b) = hand == h && type == t && bid == b

||| Card strength order defined for task 1, strongest first.
CARD_ORDER1 : List Char
CARD_ORDER1 = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2']

||| Card strength order defined for task 2, strongest first.
CARD_ORDER2 : List Char
CARD_ORDER2 = ['A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J']

||| Potential replacement cards for the joker cards in task 2.
JOKER_WILDS : List Char
JOKER_WILDS = ['A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2']

||| Compare two lists of cards by card strength according to the order given (strongest first).
||| Assumes both lists are equally sized.
compareHandOrder : (order: List Char) -> List Char -> List Char -> Ordering
compareHandOrder order [] ys = EQ -- both xs & ys are empty at the same time but this avoid dealing with partial.
compareHandOrder order xs [] = EQ
compareHandOrder order (x :: xs) (y :: ys) = let xi = fromMaybe (-1) $ findIndex (==x) order
                                                 yi = fromMaybe (-1) $ findIndex (==y) order
                                       in case compare xi yi of
                                               LT => GT -- lower index means stronger card
                                               GT => LT -- higher index means weaker card
                                               EQ => compareHandOrder order xs ys

||| Orders hands by kind and then by order of the cards in the hand.  Weakest hand is lowest.
Ord Hand where
  compare (MkHand hand type bid) (MkHand h t b) = case compare type t of
                                                       EQ => compareHandOrder CARD_ORDER1 hand h
                                                       LT => LT
                                                       GT => GT

||| Orders hands by kind then by task 2 order of cards in hand.  Weakest hand is lowest.
[task2ord] Ord Hand where
  compare (MkHand hand type bid) (MkHand h t b) = case compare type t of
                                                       EQ => compareHandOrder CARD_ORDER2 hand h
                                                       LT => LT
                                                       GT => GT


||| What is the highest possible hand type from this hand of cards?
||| Assumes the Vect is sorted so the same cards are adjacent.
||| `order` is the order in which cards are ranked as a high card.
partial
extractType : (order: List Char) -> List Char -> HandType
extractType order vs@[a,b,c,d,e] = let spans = group vs
                                       largestFirst = reverse $ sortBy (\xs, ys => compare (length xs) (length ys)) spans
                                   in case length largestFirst of
                                     1 => FiveKind a
                                     5 => extractHighCard largestFirst
                                     4 => extractOnePair largestFirst
                                     2 => extractFourHighOrFullhouse largestFirst
                                     _ => extract3Group largestFirst
  where
    extractOnePair : List (List1 Char) -> HandType
    extractOnePair [as, bs, cs, ds] = OnePair (head as)

    extractFourHighOrFullhouse : List (List1 Char) -> HandType
    extractFourHighOrFullhouse [as, bs] = if length as == 4
                                          then FourKind (head as)
                                          else FullHouse (head as) (head bs)
                              
    -- The argument is order with the largest group size first, not the highest card value first!
    -- Not sure why the compiler needs the explicit type indications with the Char list.
    extractHighCard : List (List1 Char) -> HandType
    extractHighCard xss = let hand = (the (List Char) [a,b,c,d,e]) -- quicker to pull the hand from the outer fn.
                              hasCard = map (\x => elem x hand) order
                              hasCard' = zip hasCard order
                              res = find fst hasCard'
                          in case res of
                                  Nothing => idris_crash "Somehow have no card when extracting high card."
                                  Just (_, c) => HighCard c
    
    -- There are different combinations of exactly 3 groups to check for:
    -- ThreeKind + HighCard (+ other card)
    -- OnePair + OnePair + HighCard (ie, TwoPair + HighCard)
    extract3Group : List (List1 Char) -> HandType
    extract3Group [as, bs, cs] = if length as == 3
                                 then ThreeKind (head as)
                                 else TwoPair (head as) (head bs)

parseLine : String -> (String, Int)
parseLine str = let (h, t) = span (/= ' ') str
                in (h, fromMaybe (-1) $ parsePositive (drop 1 t))

parseFile : List String -> List (String, Int)
parseFile strs = map parseLine strs

-- for debugging; surprised an `unwords` version isn't in base.
Show a => Cast (List a) String where
  cast xs = "[" ++ (unlines (map show xs)) ++ "]"

partial
task1 : List String -> Maybe Int
task1 strs = let dat = parseFile strs
                 dat' = map (\(h,t)=> (unpack h, t)) dat
                 hnds = map (\(h,t)=> MkHand h (extractType CARD_ORDER1 $ sort h) t) dat'
                 hnds' = sort hnds -- weakest hand will now be index 0.
                 hnds'' = zip [1..(cast $ length hnds')] hnds'
                 tot = sum $ map (\(i, h) => i * h.bid) hnds''
             in Just tot


||| What best hand can this make if any potential J's are substituted with any other card?
||| Assumes Vect is sorted so the same cards are adjacent.
partial
optimalExtractType : List Char -> HandType
optimalExtractType hand = let opts = map (\x => sort $ replaceOn 'J' x hand) JOKER_WILDS
                              opts' = map (extractType CARD_ORDER2) opts
                              bests = sort opts'  -- Note compaing HandType (which is the same between tasks).
                          in case last' bests of
                                  Just x => x
                                  Nothing => idris_crash "optimalExtractType: should always have at least 1!"

partial
task2 : List String -> Maybe Int
task2 strs = let dat = parseFile strs
                 dat' = map (\(h,t)=> (unpack h, t)) dat
                 hnds = map (\(h,t)=> MkHand h (optimalExtractType $ sort h) t) dat'
                 hnds' = sort @{task2ord} hnds -- weakest hand will now be index 0.
                 hnds'' = zip [1..(cast $ length hnds')] hnds'
                 tot = sum $ map (\(i, h) => i * h.bid) hnds''
             in Just tot


--------------------------------------------------------------------------------
partial
main1 : IO()
main1 = withLines' "day07-input.txt" task1

partial
main2 : IO()
main2 = withLines' "day07-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
partial
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
