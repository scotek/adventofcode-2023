module Day03

import System
import AOCUtils
import Matrix
import Data.List
import Data.Vect
import Data.List1
import Data.String
import Data.String.Extra


Coord : Type
Coord = (Int, Int)

data ItemType = Symbol | Number

Eq ItemType where
  (==) Symbol Symbol = True
  (==) Number Number = True
  (==) _ _ = False

Item : Type
Item = (Int, ItemType, String, List Coord)

||| Parse an individual line.  xCoord should start as 0 to keep track of current position in the string.
parseLine : (str: List Char) -> (xCoord: Int) -> (yCoord: Int) -> List Item
parseLine [] xCoord yCoord = []
parseLine ('.' :: xs) xCoord yCoord = parseLine xs (xCoord+1) yCoord
parseLine (x :: xs) xCoord yCoord
  = if isDigit x
    then
      -- extract the entire number then skip it
      let numLen = 1 + calcNumLen xs
          numStr = pack $ take (cast numLen) (x::xs)
          restStr = drop (cast (numLen - 1)) xs
          coordList = [ (xCoord + i, yCoord) | i <- [0..(numLen-1)] ]
          newel = (yCoord * 1000 + xCoord, Number, numStr, coordList)
      in newel :: parseLine restStr (xCoord+numLen) yCoord
    else
      -- must be a symbol; symbols can only be 1 char long
      (yCoord * 1000 + xCoord, Symbol, cast x, [(xCoord, yCoord)]) :: parseLine xs (xCoord+1) yCoord
  where
    -- How long is the prefix of digits?
    calcNumLen : List Char -> Int
    calcNumLen [] = 0
    calcNumLen (x::xs) = if isDigit x then 1 + calcNumLen xs else 0


||| Parse each line of the file and concat all the discovered items into one list.
parseFile : List String -> List Item
parseFile strs = concat $ parseFile' (map unpack strs) 0
  where
    parseFile' : List (List Char) -> (yCoord: Int) -> List (List Item)
    parseFile' [] _ = []
    parseFile' (x :: xs) y = parseLine x 0 y :: parseFile' xs (y+1)

||| Generate coordinates of 8 points around this given point, removing any <0 or >140.
surroundingCoords : Coord -> List Coord
surroundingCoords (x, y) = catMaybes $ [ c(x-1, y-1), c(x, y-1), c(x+1, y-1),
                                         c(x-1, y),              c(x+1, y),
                                         c(x-1, y+1), c(x, y+1), c(x+1, y+1)]
  where
    c : Coord -> Maybe Coord
    c (x,y) = if x < 0 || y < 0 || x > 140 || y > 140 then Nothing else Just (x,y)


||| Give a target Item, return all the items that have at least one 'digit' adjacent.
||| If an item is adjacent in multiple digits then it will still only return the item once.
identifyAdjacent : List Item -> (target: Item) -> List Item
identifyAdjacent [] target = []
identifyAdjacent (x@(_, _, _, its) :: xs) target@(_, _, _, cs)
  = let surCords = (surroundingCoords (fromMaybe (-1,-1) (getAt 0 cs)))
        inter = intersect surCords its -- check surrounding coords
    in if length inter == 0
       then
         identifyAdjacent xs target
       else
         x :: identifyAdjacent xs target


task1 : List String -> Maybe Int
task1 strs = let items = parseFile strs
                 syms = filter (\(_, t, _, _) => t == Symbol) items
                 nums = filter (\(_, t, _, _) => t == Number) items
                 -- for each symbol, get the adjacent nums
                 allAdjNums = map (identifyAdjacent nums) syms
                 allAdjNums' = concat allAdjNums
                 -- then extract all of those numbers and sum them
                 tot = map (\(_, _, str, _)=> parsePositive str) allAdjNums'
                 tot' = sum $ catMaybes tot
             in Just $ tot'
    


task2 : List String -> Maybe Int
task2 strs = let items = parseFile strs
                 syms = filter (\(_, t, s, _) => t == Symbol && s == "*") items
                 nums = filter (\(_, t, _, _) => t == Number) items
                 -- for each symbol, get the adjacent nums and keep the only ones with 2 nums adj
                 allAdjNums = map (identifyAdjacent nums) syms
                 allAdjNums' = filter (\xs=> length xs == 2) allAdjNums
                 -- multiply the 2-element pairs then sum them all, complicated slightly by needing to keep the
                 -- 2-element list together until the multiply before the sum.
                 ns = map (map (\(_, _, str, _)=> parsePositive str)) allAdjNums'
                 ns' = map (catMaybes) ns
                 prods = map product ns'
                 tot = sum prods
             in Just $ tot

--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day03-input.txt" task1

main2 : IO()
main2 = withLines' "day03-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
