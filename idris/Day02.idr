module Day02

import System
import AOCUtils
import Data.List
import Data.Vect
import Data.List1
import Data.String
import Data.String.Extra

||| Represent a round as a 3-element vector rather than individual elements to make it easy to map over.
Round : Type
Round = Vect 3 Int              -- 0 blue, 1 green, 2 red

||| An individual game has an unknown number of rounds.
Game : Type
Game = List Round

||| A match is just a list of games.  Not realy used beyond the top-level parsing function.
Match : Type
Match = List Game

||| Parse each color in a round string, returning a Round with the total cubes across the 3 choices.
parseColours : List String -> Vect 3 Int -> Vect 3 Int
parseColours [] acc = acc
parseColours (x :: xs) acc = let (numStr, col) = break (==' ') x
                                 num = fromMaybe 0 $ parsePositive numStr
                                 tmp = case trim col of
                                           "blue" => [0, 0, num]
                                           "green" => [0, num, 0]
                                           "red" => [num, 0, 0]
                                           _ => [0,0,0] -- can't happen unless input malformed
                                 acc' = zipWith (+) acc tmp
                             in parseColours xs acc'

||| Colours could be in any order in the string, if present at all.
||| Eg, `parseRound " 3 blue, 10 red" => [10, 0, 3]`
parseRound : String -> Round
parseRound rndstr = let bits = split (==',') rndstr
                        bits' = map trim bits
                        cs = parseColours (forget bits') [0,0,0]
                    in cs

||| Parse an entire game line, made up of multiple rounds.
||| Does not store the game number.
parseGame : String -> Game
parseGame str = let (hdr, strs) = break (==':') str
                    rnds = Data.String.split (==';') (drop 1 strs)
                    rnds' = map parseRound rnds
                in forget rnds'

||| Parse a list representing all of the games in the match.
parseMatch : List String -> Match
parseMatch xs = map parseGame xs

||| Are all the elements of `vec` pair-wise <= to limit?
withinLimit: (limit: Vect 3 Int) -> (vec: Vect 3 Int) -> Bool
withinLimit [a1,b1,c1] [a2,b2,c2] = a2 <= a1 && b2 <= b1 && c2 <= c1
-- Could use higher-order functions as below for an arbitrary length Vect,
-- but have the problem with Lazy and non-lazy not being compatible.
--withinLimit lim vec = and $ zipWith (<=) vec lim
        
||| True if none of the colour's cube counts exceed the limit in any of the rounds of an individual game.
gameDoesntExceedLimit : (limit: Vect 3 Int) -> Game -> Bool
gameDoesntExceedLimit lim xs = let a = map (withinLimit lim) xs
                                   a' = foldr (&&-) True a
                               in a'


task1 : List String -> Maybe Int
task1 strs = let m = parseMatch strs
                 m' = map (gameDoesntExceedLimit [12, 13, 14]) m
                 m'' = zip [1..(length m')] m'
                 pos = Prelude.List.filter ((==True) . snd) m''
                 tot = foldr (\(a,b),y=>a+y) Z pos
             in Just (cast {to=Int} tot)


||| Return the maximum number of each individual cube colour in a round through the whole game.
gameMaxCubes : Game -> Vect 3 Int
gameMaxCubes rnds = let ms = foldr (fn) [0,0,0] rnds  -- instead of writing (fn) we could use (ZipWith max).
                    in ms
  where
    fn : (el: Vect 3 Int) -> (acc: Vect 3 Int) -> Vect 3 Int
    fn [r,g,b] [mr,mg,mb] = [max r mr, max g mg, max b mb]
    

task2 : List String -> Maybe Int
task2 strs = let m = parseMatch strs
                 m' = map gameMaxCubes m
                 ps = map (foldr (*) 1) m'
                 tot = sum ps
             in Just tot

--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day02-input.txt" task1

main2 : IO()
main2 = withLines' "day02-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
