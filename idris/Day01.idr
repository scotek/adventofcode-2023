module Day01

import System
import AOCUtils
import Data.List

-- Given a list of chars, return the first and last as a concatentated two-digit number.
extractNums : List Char -> Maybe Int
extractNums cs = do nFirst <- find isDigit cs
                    nLast <- find isDigit (reverse cs)
                    let nF = ord nFirst - ord '0' 
                    let nL = ord nLast - ord '0' 
                    Just (nF * 10 + nL)

task1 : List String -> Maybe Int
task1 strs = let str = map unpack strs
                 nums = map extractNums str
                 nums' = catMaybes nums
                 tot = sum nums'
             in Just tot


numMap : List (List Char, List Char)
numMap = map (\(a,b)=>(unpack a, unpack b)) [
  ("one", "1"),
  ("two", "2"),
  ("three", "3"),
  ("four", "4"),
  ("five", "5"),
  ("six", "6"),
  ("seven", "7"),
  ("eight", "8"),
  ("nine", "9")
]


subStrings : List Char -> List Char
subStrings xs = foldr fn xs numMap
  where
    fn : (el : (List Char, List Char)) -> (acc : List Char) -> (List Char) 
    fn (x, y) acc = substituteAll x y acc



task2 : List String -> Maybe Int
task2 strs = let str = map unpack strs
                 str' = map subStrings str
                 nums = map extractNums str'
                 nums' = catMaybes nums
                 tot = sum nums'
             in Just tot


--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day01-input.txt" task1

main2 : IO()
main2 = withLines' "day01-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
