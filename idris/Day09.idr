module Day09

import System
import AOCUtils
import Data.List
import Data.List1
import Data.Maybe
import Data.String
import Data.String.Extra
import Data.SortedMap


parseLine : String -> List Int
parseLine str = mapMaybe parseInteger (words str)

parseFile : List String -> List (List Int)
parseFile strs = map parseLine strs

||| Takes a list of numbers and return a list 1 less of the differences between adj nums.
calcDiff : List Int -> List Int
calcDiff xs = let ps = zip xs (drop 1 xs)
                  diffs = map (uncurry $ flip (-)) ps
              in diffs

||| Sum the last element of each list.
predictNext : List (List Int) -> Int
predictNext xss = let lsts = mapMaybe (last') xss
                  in foldr (+) 0 lsts


||| Calculate the series of differences until encountering a sequence all the same value.
calcDiffSeries : List Int -> List (List Int)
calcDiffSeries [] = []
calcDiffSeries xs = let ds = calcDiff xs
                    in case allSame ds of
                            True => [ds]
                            False => [ds] ++ calcDiffSeries ds

||| Do task 1 for a single entry.
task1One : List Int -> Int
task1One xs = let ds = calcDiffSeries xs
              in predictNext (xs :: ds)

task1 : List String -> Maybe Int
task1 strs = let dat = parseFile strs
                 rs = map task1One dat
                 tot = sum rs
             in Just tot



||| Predict the preceeding value from first list.
predictPrev : List (List Int) -> Int
predictPrev [] = 0
predictPrev (x :: xs) = let acc = predictPrev xs
                            h = fromMaybe (-1) $ head' x -- Nothing will never happen.
                            acc' = h - acc
                        in acc'

||| Do task 2 for a single entry.
task2One : List Int -> Int
task2One xs = let ds = calcDiffSeries xs
              in predictPrev (xs :: ds)

task2 : List String -> Maybe Int
task2 strs = let dat = parseFile strs
                 rs = map task2One dat
                 tot = sum rs
             in Just tot

--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day09-input.txt" task1

main2 : IO()
main2 = withLines' "day09-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
