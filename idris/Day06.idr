module Day06

import System
import AOCUtils
import Data.List
import Data.List1
import Data.Maybe
import Data.String
import Data.String.Extra

||| Parse a single line and just return the numbers
parseLine : String -> List Int
parseLine str = let (name, dat) = break (==':') str
                    nums = mapMaybe parsePositive (words $ drop 1 $ dat)
                in nums

||| Parse all the lines
parseFile :  List String -> List (List Int)
parseFile = map parseLine

||| How far will a boat go in the remaining time after charging for a fixed time?
calcDistForCharge : (chargeTime: Int) -> (remainingTime: Int) -> Int
calcDistForCharge chargeTime remainingTime = chargeTime * remainingTime

||| Try all the possible charging times and return the pair of the charge time and distance acheived.
calcAllDists : (totalDuration: Int) -> List (Int, Int)
calcAllDists totalDuration = let chargeTimes = [0..totalDuration]
                                 dists = map (\t => calcDistForCharge t (totalDuration-t)) chargeTimes
                             in zip chargeTimes dists

||| For a given race, how many ways are there to exceed the existing time.
waysToBeat : (Int, Int) -> Int
waysToBeat (time, dist) = let ds = calcAllDists time
                              betters = filter (\(t, d) => d > dist) ds
                          in cast $ length betters

task1 : List String -> Maybe Int
task1 strs = let dat = parseFile strs
                 times = fromMaybe [] $ getAt 0 dat
                 dists = fromMaybe [] $ getAt 1 dat
                 combined = zip times dists
                 ways = map waysToBeat combined
                 tot = product ways
             in Just tot


||| Parse a single line according to task 2's interpretation of the file format
parseLine' : String -> Int
parseLine' str = let (name, dat) = break (==':') str
                     dat' = drop 1 dat
                     numStr = pack $ filter (/=' ') (unpack dat')
                     num = fromMaybe (-1) $ parsePositive numStr
                 in num

||| Parse the whole file according to task 2's interpretation of the file format
parseFile' : List String -> (Int, Int)
parseFile' strs = let dat = map parseLine' strs
                      time = fromMaybe (0) $ getAt 0 dat
                      dist = fromMaybe (0) $ getAt 1 dat
                  in (time, dist)


task2 : List String -> Maybe Int
task2 strs = let (time, dist) = parseFile' strs
                 ways = waysToBeat (time, dist)
             in Just ways

--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day06-input.txt" task1

main2 : IO()
main2 = withLines' "day06-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
