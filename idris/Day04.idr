module Day04

import System
import AOCUtils
import Matrix
import Data.List
import Data.Vect
import Data.List1
import Data.String
import Data.String.Extra

Card : Type
Card = (Int, List Int, List Int)

||| Read in a single card.  Cheat with some error handling by ignoring any number that doesn't parse.
parseCard : String -> Card
parseCard str = let (card, body) = break (==':') str
                    (winningStr, pickedStr) = break (=='|') (drop 1 body)
                    cardNum = fromMaybe (0-1) $ parsePositive (drop 4 card)
                    winnings = mapMaybe parsePositive $ words winningStr
                    picks = mapMaybe parsePositive $ words (drop 1 pickedStr)
                in (cardNum, winnings, picks) 

parseGame : List String -> List Card
parseGame strs = map parseCard strs

countMatches : Card -> Int
countMatches (n, ws, ps) = cast $ length $ intersect ws ps



task1 : List String -> Maybe Int
task1 strs = let cards = parseGame strs
                 matches = map countMatches cards
                 matches' = filter (>0) matches
                 tots = map (\x=> pwr 2 (x - 1)) matches'
                 tot = sum tots
             in Just $ cast $ tot


||| Take a list of matches on each card and return a list of how many copies of each card.
||| (game id, matches, numCards)
cascadeCopies : List (Int, Int, Int) -> List (Int, Int, Int)
cascadeCopies [] = []
cascadeCopies ((i, m, nc) :: xs) = let xs' = updateFutureCards m nc xs
                                   in (i,m,nc) :: cascadeCopies xs'
  where
    ||| Added `extras` number of cards to the next `howFarAhead` cards.
    updateFutureCards : (howFarAhead: Int) -> (extras: Int) -> List (Int, Int, Int) -> List (Int, Int, Int)
    updateFutureCards 0 _ xs = xs
    updateFutureCards i n [] = []
    updateFutureCards i n ((xi, xm, xnc) :: xs) = (xi, xm, xnc+n) :: (updateFutureCards (i-1) n xs)



task2 : List String -> Maybe Int
task2 strs = let cards = parseGame strs
                 matches = map countMatches cards
                 copies = cascadeCopies (zip3 [1..202] matches (replicate 202 1))
                 ncs =  map (snd . snd) copies
                 tot = sum ncs
             in Just $ tot


--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day04-input.txt" task1

main2 : IO()
main2 = withLines' "day04-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
